import pytest
import argparse
from main_prog import to_int, from_int

def main():
    print("1)Вывести описание и пример ввода(желательно, если это первый запуск);\n2)Ввести значения вручную;\n3)Введите 0, чтобы выйти из программы;")
    while True:
            try:
                check = int(input("Введите 1, 2, или 0: "))
                break
            except ValueError:
                print ("Вы должны ввести число, попробуйте снова.")
    while check != 0:
        if check == 1:
            print("Для начала вас требуют ввести исходную СС, а затем новую СС, для вашего числа. Число вводится в последнюю очередь.")
            print("Пример:\nВыберите систему счисления из: 2->10, 10->2, 8->2, 8->16, 16->8: 2\nВыберите систему счисления из: 2->10, 10->2, 8->2, 8->16, 16->8: 10\nВведите число: 101")
            while True:
                try:
                    check = int(input("Введите 1, 2, или 0: "))
                    break
                except ValueError:
                    print ("Вы должны ввести число, попробуйте снова.")
        
        elif check == 2:
            while True:
                try:
                    old_sys = int(input("Выберите систему счисления из: 2->10, 10->2, 8->2, 8->16, 16->8: "))
                    new_sys = int(input("Выберите систему счисления из: 2->10, 10->2, 8->2, 8->16, 16->8: "))
                    break
                except ValueError:
                    print ("Вы должны ввести число, попробуйте снова.")
        
            if (old_sys == 2) and (new_sys == 10):
                old_num = input("Введите число: ")
                print(to_int(old_num, old_sys))

            elif (old_sys == 10) and (new_sys == 2):
                while True:
                    try:
                        old_num = int(input("Введите число: "))
                        break
                    except ValueError:
                        print ("Вы должны ввести число, попробуйте снова.")
                print(from_int(old_num, new_sys))

            elif (old_sys == 8) and (new_sys == 2):
                old_num = input("Введите число: ")
                new_num = to_int(old_num, old_sys)
                print(from_int(new_num, new_sys))

            elif (old_sys == 8) and (new_sys == 16):
                old_num = input("Введите число: ")
                new_num = to_int(old_num, old_sys)
                print(from_int(new_num, new_sys))

            elif (old_sys == 16) and (new_sys == 8):
                old_num = input("Введите число: ")
                new_num = to_int(old_num, old_sys)
                print(from_int(new_num, new_sys))
            while True:
                try:
                    check = int(input("Введите 1, 2, или 0: "))
                    break
                except ValueError:
                    print ("Вы должны ввести число, попробуйте снова.")
        else:
            print(f'Вы ввели не подходящее значение:"{check}", вам требуется ввести 1 или 2')
            while True:
                try:
                    check = int(input("Введите 1, 2, или 0: "))
                    break
                except ValueError:
                    print ("Вы должны ввести число, попробуйте снова.")
    print("До свидания...")



if __name__ == "__main__":
    main()