from main_prog import to_int, from_int
import pytest

def test_twoTen():
    assert to_int('1001', 2) == (9)
    print("Should be 1001 -> 9")

def test_tenTwo():
    assert from_int(5, 2) == ('101')
    print("Should be 5 -> 101")

def test_EightTwo():
    assert to_int('6', 8) == (6)
    assert from_int(6, 2) == ('110')
    print("Should be 6 -> 110")

def test_EightSix():
    assert to_int('25', 8) == (21)
    assert from_int(21, 16) == ('15')
    print("Should be 25 -> 15")

def test_SixEight():
    assert to_int('FF', 16) == (255)
    assert from_int(255, 8) == ('377')
    print("Should be FF -> 377")
