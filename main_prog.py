import string

def from_int(number:int, radix:int, alphabet = string.digits + string.ascii_uppercase) -> str:

    if radix > len(alphabet):
        raise ValueError('Недостаточная длина алфавита.')
        
    result = ''
    
    while number:
        result += alphabet[number % radix] 
        number //= radix
        
    if len(result):
        return result[::-1]
        
    return '0'
    
    
def to_int(number:str, radix:int, alphabet = string.digits + string.ascii_uppercase) -> int:

    if radix > len(alphabet):
        raise ValueError('Недостаточная длина алфавита.')
    
    digits_values = dict([(alphabet[i].upper(), i) for i in range(len(alphabet))])
    
    result = 0
    
    for digit, digit_index in zip(number[::-1].upper(), range(len(number))):
        result += digits_values[digit] * radix ** digit_index
        
    return result
